package za.ac.sun.cs.green.service.simplify;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConstantPropagationTest {

	@Test
	public void propagationtest1() {
		assertTrue(ConstantPropagation.propagation("x == 1 && y + x < 10").equals("y + 1 < 10"));
	}
	
	@Test
	public void propagationtest2() {
		assertTrue(ConstantPropagation.propagation("x == 1 && x + 1 > 0").equals("1 + 1 > 0"));
	}
	
	@Test
	public void simplificationtest1() {
		assertTrue(ConstantPropagation.propagation("y == 6 && x == 1 && y + x < 10").equals("6 + 1 < 10"));
	}
	
	@Test
	public void simplificationtest2() {
		fail("Not yet implemented");
	}
	
	@Test
	public void transitivetest1() {
		fail("Not yet implemented");
	}
	
	@Test
	public void transitivetest2() {
		fail("Not yet implemented");
	}

}
