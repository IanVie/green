var structpolyhedron =
[
    [ "Constraint", "structpolyhedron.html#ab2691b8eb5d2dc43a509ac2094d2bc73", null ],
    [ "Dimension", "structpolyhedron.html#a2a02cea8b7ba3dde415041b8b2373bc8", null ],
    [ "flags", "structpolyhedron.html#ac1fb864c352ef93ef3030c35e98c2aed", null ],
    [ "NbBid", "structpolyhedron.html#a9e7c80deeddf472a57bd54217f75c717", null ],
    [ "NbConstraints", "structpolyhedron.html#a2db05293e731089b9198d45cf027bd1c", null ],
    [ "NbEq", "structpolyhedron.html#af316f6440017b2e02fa3a387b2818cc9", null ],
    [ "NbRays", "structpolyhedron.html#a74cba037f5ac1c7182bf960d79d3f124", null ],
    [ "next", "structpolyhedron.html#aab97126cb9e56431d40eaf4e68b3953d", null ],
    [ "p_Init", "structpolyhedron.html#a3681618067c7bf2a0bd5e805871283f5", null ],
    [ "p_Init_size", "structpolyhedron.html#ac2aa52aede2212101bd130ba7a67663a", null ],
    [ "Ray", "structpolyhedron.html#a1332db86327e189effc8798ab230f36a", null ]
];