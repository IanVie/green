var matrix_8c =
[
    [ "hermite", "matrix_8c.html#a0913852253401fd132b31cc4a205c1de", null ],
    [ "left_hermite", "matrix_8c.html#adcbe5ee3f0c28fb35aed49f4e1f40f5b", null ],
    [ "MatInverse", "matrix_8c.html#ae2b2f031eb75c5011091536d7ef4befc", null ],
    [ "Matrix_Alloc", "matrix_8c.html#ac0b29e1d99a2823ad00b5f2157879d80", null ],
    [ "Matrix_Extend", "matrix_8c.html#a22b92aad923f05a327d06d761f0396e0", null ],
    [ "Matrix_Free", "matrix_8c.html#afcb312b7c12a6997cd66964ecc34e1a6", null ],
    [ "Matrix_Inverse", "matrix_8c.html#a97aa755c011357ce2146d71ddb88ded6", null ],
    [ "Matrix_Print", "matrix_8c.html#ad4c3c350dba633f72bbcf3609b6b67d7", null ],
    [ "Matrix_Product", "matrix_8c.html#a1e967c74adaa2eec868fd737a55b6624", null ],
    [ "Matrix_Read", "matrix_8c.html#a3a087ae9a03d5baf0b81831177931143", null ],
    [ "Matrix_Read_Input", "matrix_8c.html#a40c219a9893d0f0ea7f9448ad27fcc3d", null ],
    [ "Matrix_Vector_Product", "matrix_8c.html#a4623e4f52b5c7b43d38dfb14015781ce", null ],
    [ "rat_prodmat", "matrix_8c.html#adb8526f2ba863572c60b255c8b27c186", null ],
    [ "right_hermite", "matrix_8c.html#a9d112ac078583626580f848b51a515f3", null ],
    [ "Vector_Matrix_Product", "matrix_8c.html#a15129f9f57a6a92234f4226cb71bcb39", null ]
];