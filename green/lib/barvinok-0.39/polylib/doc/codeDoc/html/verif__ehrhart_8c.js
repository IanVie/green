var verif__ehrhart_8c =
[
    [ "BIGDIM", "verif__ehrhart_8c.html#a8280de28f23f81d202ac9b2436ac0eb8", null ],
    [ "MAXRAYS", "verif__ehrhart_8c.html#a89fd83aa168651629c012d8655635588", null ],
    [ "RANGE", "verif__ehrhart_8c.html#ac04dd0afaf7ea3eb2ade2544d2d5f907", null ],
    [ "SRANGE", "verif__ehrhart_8c.html#a0529c3d24b479e7b3421e2ed293aa197", null ],
    [ "VBIGDIM", "verif__ehrhart_8c.html#a4417e623e485f606d7b344cf34aa7ecb", null ],
    [ "VSRANGE", "verif__ehrhart_8c.html#a387713ee70775fb2114973c6212a1c42", null ],
    [ "check_poly", "verif__ehrhart_8c.html#adc96f6810690bdfed45afff2739f93ee", null ],
    [ "main", "verif__ehrhart_8c.html#a0ddf1224851353fc92bfbff6f499fa97", null ],
    [ "max", "verif__ehrhart_8c.html#a76af106aaa860ca001b28491fa8e046c", null ],
    [ "min", "verif__ehrhart_8c.html#a1c0ed9242a98f138b59c76f8454cfaa6", null ],
    [ "st", "verif__ehrhart_8c.html#a35b567772b277e799ec7be04de19b2e7", null ]
];