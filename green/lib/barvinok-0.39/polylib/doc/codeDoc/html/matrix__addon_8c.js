var matrix__addon_8c =
[
    [ "constraintsView", "matrix__addon_8c.html#a495de3cced5c0eecf74aef91b05502dd", null ],
    [ "constraintsView_Free", "matrix__addon_8c.html#aa0cc9da9320a0f253db72e78f4f6e56b", null ],
    [ "eliminate_var_with_constr", "matrix__addon_8c.html#a79d19aa329285819b31bc7a602241842", null ],
    [ "Identity_Matrix", "matrix__addon_8c.html#a33f64eb039ac943c1bddbafc3be3ece6", null ],
    [ "Matrix_clone", "matrix__addon_8c.html#a8269bc80b6beba6d58a4c5b029f04bbf", null ],
    [ "Matrix_copySubMatrix", "matrix__addon_8c.html#acb90cceb8e7a674a3d5796a1402f0c9e", null ],
    [ "Matrix_identity", "matrix__addon_8c.html#adb1631ef2ddb3b386742b881b30f8ce6", null ],
    [ "Matrix_oppose", "matrix__addon_8c.html#acbbded0c02e89b1a7b6b08ee3f396986", null ],
    [ "Matrix_subMatrix", "matrix__addon_8c.html#a33fec91bb879b7b4dda814ad9d33f41b", null ],
    [ "mpolyhedron_compress_last_vars", "matrix__addon_8c.html#a44f487e1b38f0f47c115d2115087a017", null ],
    [ "mpolyhedron_deflate", "matrix__addon_8c.html#a0f0ba73176f5316a02e849b8c210204a", null ],
    [ "mpolyhedron_eliminate_first_variables", "matrix__addon_8c.html#a649c9050ca9d6229d5b48e966da1f35c", null ],
    [ "mpolyhedron_inflate", "matrix__addon_8c.html#ac4b68d480196756e0deffafb3d99ecc7", null ],
    [ "mpolyhedron_simplify", "matrix__addon_8c.html#ace44097b2d9d40d05934971b9716af89", null ],
    [ "mtransformation_expand_left_to_dim", "matrix__addon_8c.html#a0fd41929c8927ab1ce2ec603fe283865", null ],
    [ "mtransformation_inverse", "matrix__addon_8c.html#ab736f7ec39f7b625397e965e8b612ad2", null ],
    [ "split_constraints", "matrix__addon_8c.html#a288db1ffeb5425aada82cabf5c824bc4", null ]
];