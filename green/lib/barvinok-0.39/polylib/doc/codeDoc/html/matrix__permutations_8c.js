var matrix__permutations_8c =
[
    [ "Constraints_permute", "matrix__permutations_8c.html#a1714c230c3b1785a1da3c38bcbf7e661", null ],
    [ "eliminable_vars", "matrix__permutations_8c.html#a46efd59d133aa22046bd57718c7be05a", null ],
    [ "find_a_permutation", "matrix__permutations_8c.html#af43c186b57b2df6caa02a812c175a4d1", null ],
    [ "mpolyhedron_permute", "matrix__permutations_8c.html#ae642907b1f7aac2f4ec8f174b27ab830", null ],
    [ "mtransformation_permute", "matrix__permutations_8c.html#ace85f1b44be83dc08923e3a7bc66804c", null ],
    [ "nb_bits", "matrix__permutations_8c.html#a9258ae89e8c6c38ba0a82824cd790fef", null ],
    [ "permutation_for_full_dim2", "matrix__permutations_8c.html#a2969569d57d59249e748e075e2494789", null ],
    [ "permutation_inverse", "matrix__permutations_8c.html#afbe2c7462b899ca85c30140ac5403089", null ]
];