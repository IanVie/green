var types_8h =
[
    [ "Vector", "structVector.html", "structVector" ],
    [ "matrix", "structmatrix.html", "structmatrix" ],
    [ "polyhedron", "structpolyhedron.html", "structpolyhedron" ],
    [ "interval", "structinterval.html", "structinterval" ],
    [ "_Param_Vertex", "struct__Param__Vertex.html", "struct__Param__Vertex" ],
    [ "_Param_Domain", "struct__Param__Domain.html", "struct__Param__Domain" ],
    [ "_Param_Polyhedron", "struct__Param__Polyhedron.html", "struct__Param__Polyhedron" ],
    [ "_evalue", "struct__evalue.html", "struct__evalue" ],
    [ "_enode", "struct__enode.html", "struct__enode" ],
    [ "_enumeration", "struct__enumeration.html", "struct__enumeration" ],
    [ "LatticeUnion", "structLatticeUnion.html", "structLatticeUnion" ],
    [ "ZPolyhedron", "structZPolyhedron.html", "structZPolyhedron" ],
    [ "emptyQ", "types_8h.html#a156641ae25106f9a6ac5c5c2a624c168", null ],
    [ "END_FORALL_PVertex_in_ParamPolyhedron", "types_8h.html#a670b1ea03d880e2595bddca5745df8ec", null ],
    [ "F_CLR", "types_8h.html#abf7ccc8dc487c568afdb1efd664d1e61", null ],
    [ "F_INIT", "types_8h.html#a5283dcd004bd2fcfaab76b89c6c9322b", null ],
    [ "F_ISSET", "types_8h.html#a19f1ae711bb0e3063ba1962cb8a898a0", null ],
    [ "F_SET", "types_8h.html#ab04b7b56e2789336fbc544896cb477f9", null ],
    [ "FIRST_PARAMETER_NAME", "types_8h.html#a0983cb88d8aefa8ac53282de30590726", null ],
    [ "FL_CLR", "types_8h.html#a287b3b51b0f3bf633e9f00d23b69da46", null ],
    [ "FL_INIT", "types_8h.html#acd3e6da08aea98ada4bad87b9a63a4e0", null ],
    [ "FL_ISSET", "types_8h.html#af6497c16618b94a803a1b43f31629f9c", null ],
    [ "FL_SET", "types_8h.html#ad564ff71777a709191756eacebd5721a", null ],
    [ "FORALL_PVertex_in_ParamPolyhedron", "types_8h.html#aea9236e3c02c25e1cb628684409630fc", null ],
    [ "FOREVER", "types_8h.html#a75c828ed6c02fcd44084e67a032e422c", null ],
    [ "LB_INFINITY", "types_8h.html#ab3e4132829e88232125e7bf259520a18", null ],
    [ "MAXNOOFRAYS", "types_8h.html#a7a3726264ecf2f0985882e0c1adb4ddb", null ],
    [ "MSB", "types_8h.html#a11f27b3b63dfc15c4b4fbb665a3ce3fe", null ],
    [ "NEXT", "types_8h.html#a3dd66db5db700555d4e24314974a2ea1", null ],
    [ "P_VALUE_FMT", "types_8h.html#ae6f16bcd4a42ba51cbb003e3d1e1cde6", null ],
    [ "PCHAR", "types_8h.html#a39e85cb3471b4808b39bdde4fb125cbe", null ],
    [ "POL_FACETS", "types_8h.html#adef192e3530c5e1fcb2c44858a78f703", null ],
    [ "POL_HIGH_BIT", "types_8h.html#a827ee0a650c714bebc02dfa7da978b5f", null ],
    [ "POL_INEQUALITIES", "types_8h.html#a1869e11b8e9a1e9e89e8145c242e0eea", null ],
    [ "POL_INTEGER", "types_8h.html#ad8b11210e90424f4d7be767b93ae2f72", null ],
    [ "POL_ISSET", "types_8h.html#a223261a4719dd67ef2048c192bb34099", null ],
    [ "POL_NO_DUAL", "types_8h.html#a754586a0aee88f21ad4c98b1cddee1be", null ],
    [ "POL_POINTS", "types_8h.html#aacccb2db7ed6cb45d5e324216aa5ea3f", null ],
    [ "POL_VALID", "types_8h.html#a2ac4c28a3acbe4578e03ad20db7939cc", null ],
    [ "POL_VERTICES", "types_8h.html#a18cb4aab2aec424306ea8f67f6dfc9b0", null ],
    [ "POLY_UNION_OR_STRUCT", "types_8h.html#aa4be88adae6068f5be6a24cfc1bfc9ba", null ],
    [ "TOP", "types_8h.html#afc0eef637f1016e8786e45e106a4881e", null ],
    [ "UB_INFINITY", "types_8h.html#a629bf0301263815e35636f493380b9dd", null ],
    [ "universeQ", "types_8h.html#a355595bc9326db6a3ec78b2e5c2a0f97", null ],
    [ "enode", "types_8h.html#aaf5a68294151c599018da3c09c68f2f1", null ],
    [ "Enumeration", "types_8h.html#ad661e40acbb0cab5747f7895e281eb90", null ],
    [ "evalue", "types_8h.html#aa7f6151bd790512d5f0c9096d5154acc", null ],
    [ "Interval", "types_8h.html#abdec5cbaa42edc69447cbf1a9f4ab229", null ],
    [ "Lattice", "types_8h.html#ac8d4befd62b1ee6f70e665b1db3192cf", null ],
    [ "LatticeUnion", "types_8h.html#a0e342aadb60685bd1c24fa0e852593e9", null ],
    [ "Matrix", "types_8h.html#a1464209c2a3939f0c3d26c87f04d25a6", null ],
    [ "Param_Domain", "types_8h.html#a080e5e2318fdc37b660e03aa7baf87e5", null ],
    [ "Param_Polyhedron", "types_8h.html#a7956721a2f75440b8abbfbd20436f213", null ],
    [ "Param_Vertices", "types_8h.html#a24fc421f6cabaf18feb4892ff558d644", null ],
    [ "Polyhedron", "types_8h.html#a14dce80aeba07d46cca3aaf45667a9be", null ],
    [ "ZPolyhedron", "types_8h.html#a1cf4f45e61e77ed45a51e6ddf1d6bd41", null ],
    [ "Bool", "types_8h.html#a39db6982619d623273fad8a383489309", [
      [ "False", "types_8h.html#a39db6982619d623273fad8a383489309af9d46b7804d93a4fcde88489a1b68c24", null ],
      [ "True", "types_8h.html#a39db6982619d623273fad8a383489309a6d32c34708a0a3507c4bdb89219d650b", null ]
    ] ],
    [ "enode_type", "types_8h.html#a01e5f42033d0f34c1a182a25a7409fed", [
      [ "polynomial", "types_8h.html#a01e5f42033d0f34c1a182a25a7409fedaa486649798da33c51389df14eaccd3ce", null ],
      [ "periodic", "types_8h.html#a01e5f42033d0f34c1a182a25a7409feda3cb930eb7594d2236ab4fc6dd622c207", null ],
      [ "evector", "types_8h.html#a01e5f42033d0f34c1a182a25a7409fedacabe6900b0394de2166c488c664bdd6a", null ]
    ] ],
    [ "Pol_status", "types_8h.html#aa587bf4e8c763fa5e95542df7eb070b7", null ]
];