var arithmetic__errors_8h =
[
    [ "__CURRENT_FUNCTION_NAME__", "arithmetic__errors_8h.html#afb9d6e881bafb0485a90b7c3c7bfe46c", null ],
    [ "CATCH", "arithmetic__errors_8h.html#a221261b286628ee622df9588446d503e", null ],
    [ "EXCEPTION", "arithmetic__errors_8h.html#a8e5fdcdfe948aeed8558f0d81392e941", null ],
    [ "RETHROW", "arithmetic__errors_8h.html#ac8d720dd87a0d11d335c873336c65cee", null ],
    [ "THROW", "arithmetic__errors_8h.html#a748af154884693013ee5f03878988dbe", null ],
    [ "TRY", "arithmetic__errors_8h.html#ad2746371528bdf15c3910b7bf217dac0", null ],
    [ "UNCATCH", "arithmetic__errors_8h.html#a8211465bb59b0e6b8c374b440965dbb4", null ],
    [ "exception_callback_t", "arithmetic__errors_8h.html#aafc39b4cb7a07fefadbdaeb9f8753679", null ],
    [ "the_last_just_thrown_exception", "arithmetic__errors_8h.html#a9dbd1094abce0128b2995c91d701349d", null ]
];