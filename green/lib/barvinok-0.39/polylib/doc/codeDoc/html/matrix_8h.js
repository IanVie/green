var matrix_8h =
[
    [ "left_hermite", "matrix_8h.html#adcbe5ee3f0c28fb35aed49f4e1f40f5b", null ],
    [ "MatInverse", "matrix_8h.html#a3fd6efb2ec63815561e0b8f143c6191f", null ],
    [ "Matrix_Alloc", "matrix_8h.html#ac0b29e1d99a2823ad00b5f2157879d80", null ],
    [ "Matrix_Extend", "matrix_8h.html#a22b92aad923f05a327d06d761f0396e0", null ],
    [ "Matrix_Free", "matrix_8h.html#afcb312b7c12a6997cd66964ecc34e1a6", null ],
    [ "Matrix_Inverse", "matrix_8h.html#a97aa755c011357ce2146d71ddb88ded6", null ],
    [ "Matrix_Print", "matrix_8h.html#ad4c3c350dba633f72bbcf3609b6b67d7", null ],
    [ "Matrix_Product", "matrix_8h.html#ae08cf1c743e8b893f07a51a57e2cb1a2", null ],
    [ "Matrix_Read", "matrix_8h.html#a3a087ae9a03d5baf0b81831177931143", null ],
    [ "Matrix_Read_Input", "matrix_8h.html#a40c219a9893d0f0ea7f9448ad27fcc3d", null ],
    [ "Matrix_Vector_Product", "matrix_8h.html#a5c3c6a49794ba7b1c6f0e39729f6af56", null ],
    [ "rat_prodmat", "matrix_8h.html#adb8526f2ba863572c60b255c8b27c186", null ],
    [ "right_hermite", "matrix_8h.html#a9d112ac078583626580f848b51a515f3", null ],
    [ "Vector_Matrix_Product", "matrix_8h.html#a314e578183a3d3f48f56a2b1a5637f15", null ]
];