var homogenization_8c =
[
    [ "dehomogenize_enode", "homogenization_8c.html#af91fee5e1af870b385e88405cf116652", null ],
    [ "dehomogenize_enumeration", "homogenization_8c.html#a06456b946001b8ddaf7203de4856abd7", null ],
    [ "dehomogenize_evalue", "homogenization_8c.html#aad8cd6181d26eb3f13171462ee30b932", null ],
    [ "dehomogenize_periodic", "homogenization_8c.html#af7a29828963075a67e9d7e95325ee972", null ],
    [ "dehomogenize_polyhedron", "homogenization_8c.html#abab952bcd8adf0cc6af6a70823648266", null ],
    [ "dehomogenize_polynomial", "homogenization_8c.html#aca605e4d7473dda2b7499e9f7fb44f53", null ],
    [ "homogenize", "homogenization_8c.html#afc536ef283eb21ce3e2cbcd450d72969", null ]
];