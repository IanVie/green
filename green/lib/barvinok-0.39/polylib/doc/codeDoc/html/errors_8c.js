var errors_8c =
[
    [ "linear_exception_holder", "structlinear__exception__holder.html", "structlinear__exception__holder" ],
    [ "exception_debug_message", "errors_8c.html#a8d4a2127a1edadca61c0631735922769", null ],
    [ "exception_debug_trace", "errors_8c.html#a82fa865508693149b63d670fa7d7c438", null ],
    [ "FALSE", "errors_8c.html#aa93f0eb578d23995850d61f7d61c55c1", null ],
    [ "MAX_STACKED_CONTEXTS", "errors_8c.html#aaec70e82a04c1102db5789b0c8092055", null ],
    [ "same_string_p", "errors_8c.html#ac32f3590ee78925a812d8718f28e9a2b", null ],
    [ "TRUE", "errors_8c.html#aa8cecfc5c5c054d2875c03e77b7be15d", null ],
    [ "boolean", "errors_8c.html#a621c38f1f10a1c565d897e3178b16d6e", null ],
    [ "dump_exception_stack", "errors_8c.html#a14b32c4f73df3d2d17000b401db5d88a", null ],
    [ "dump_exception_stack_to_file", "errors_8c.html#a528a38285a81d0501a9942a7941163ab", null ],
    [ "get_exception_name", "errors_8c.html#a79edc2c9a0ce7b3159417f9130a14def", null ],
    [ "linear_initialize_exception_stack", "errors_8c.html#a0b645a3ab909b237aebbf6b31560705c", null ],
    [ "pop_exception_from_stack", "errors_8c.html#af00ac00cc6c96f681274c7d1743e034a", null ],
    [ "push_exception_on_stack", "errors_8c.html#a6506ceda3a14deda94141a42d0b3ae4b", null ],
    [ "set_exception_callbacks", "errors_8c.html#a93de4c04224b53c462a8bd9242edd3ed", null ],
    [ "throw_exception", "errors_8c.html#a328c6d73c9b785c2d853434eeac83883", null ],
    [ "any_exception_error", "errors_8c.html#a3fbedc713824754a33dd94278851fe70", null ],
    [ "exception_index", "errors_8c.html#ad63e2e9e42900784d7f912727fc90af3", null ],
    [ "exception_stack", "errors_8c.html#ae7d6c7530e7505f175ee1efaf0dd33e1", null ],
    [ "linear_exception_debug_mode", "errors_8c.html#a0471cbbdd66a5ee1fa3a459878c756be", null ],
    [ "linear_exception_verbose", "errors_8c.html#a3e54f04e8bd9d204f2ad4dcd32ac744f", null ],
    [ "linear_number_of_exception_thrown", "errors_8c.html#a3313c41a656ee03e88a9d00234cfc5ec", null ],
    [ "overflow_error", "errors_8c.html#af74135a8750c28205f819842ae8be9c9", null ],
    [ "parser_exception_error", "errors_8c.html#a3806d9a2c0134fd8bef8c192341a8954", null ],
    [ "pop_callback", "errors_8c.html#a5cb3be87cd274e085213bafc6eca475f", null ],
    [ "push_callback", "errors_8c.html#a949dce609af01654ccefedf9473eac29", null ],
    [ "simplex_arithmetic_error", "errors_8c.html#a381d67b7ee03ff84c242d6f342fa063c", null ],
    [ "the_last_just_thrown_exception", "errors_8c.html#a9dbd1094abce0128b2995c91d701349d", null ],
    [ "timeout_error", "errors_8c.html#a3cae90a91b95e5365e192179877cccf9", null ],
    [ "user_exception_error", "errors_8c.html#a18b35523d2702706b86f2ab8ae2393d2", null ]
];