var Matop_8h =
[
    [ "AddANullColumn", "Matop_8h.html#a7d43e9c44b8c49cd9a542102042ad72e", null ],
    [ "AddANullRow", "Matop_8h.html#a76461087aa1a40441a3dc174e8359b97", null ],
    [ "ExchangeColumns", "Matop_8h.html#af9321ba930c53a16bb88667689268d5d", null ],
    [ "ExchangeRows", "Matop_8h.html#acf8ce28d22ae1fabdf97ac6c4e86341a", null ],
    [ "findHermiteBasis", "Matop_8h.html#ad77e679cec7ff5e82f9d09d52e92db06", null ],
    [ "Identity", "Matop_8h.html#ac886b07103118bccddf1693e2bb9655f", null ],
    [ "isinHnf", "Matop_8h.html#a3f47c6fa59a6178c86d3990bb25af47b", null ],
    [ "isIntegral", "Matop_8h.html#ab11e2b3f499da1c15918d52e690cfbd1", null ],
    [ "Lcm", "Matop_8h.html#a13dc27a49cfaf5367846f015b76c2840", null ],
    [ "Lcm3", "Matop_8h.html#a69ef60460903eaa2378b40e13181e565", null ],
    [ "Matrix_Copy", "Matop_8h.html#a9d027e9fc6b85e6fa37fc284bf1b5e06", null ],
    [ "PutColumnFirst", "Matop_8h.html#abff6804a65715b1def03cf02c07d138c", null ],
    [ "PutColumnLast", "Matop_8h.html#a5912c5c3befc11a92877a93969f359f6", null ],
    [ "PutRowFirst", "Matop_8h.html#ab6ddbe687809022ebaf4ff691adbb48d", null ],
    [ "PutRowLast", "Matop_8h.html#a437f63cf9b5f8caea2b5dd4b27f9a28a", null ],
    [ "RemoveColumn", "Matop_8h.html#a7866f34e302caaf90becd08b1245c126", null ],
    [ "RemoveNColumns", "Matop_8h.html#a4b8fff8126ca3b066999fd338c0ba023", null ],
    [ "RemoveRow", "Matop_8h.html#a4ef5342bc70d9c63714d1092a7668b03", null ],
    [ "Transpose", "Matop_8h.html#ab635a9108899776e30e1de03fe183915", null ]
];