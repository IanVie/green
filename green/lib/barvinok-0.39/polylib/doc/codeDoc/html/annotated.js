var annotated =
[
    [ "_enode", "struct__enode.html", "struct__enode" ],
    [ "_enumeration", "struct__enumeration.html", "struct__enumeration" ],
    [ "_evalue", "struct__evalue.html", "struct__evalue" ],
    [ "_Param_Domain", "struct__Param__Domain.html", "struct__Param__Domain" ],
    [ "_Param_Polyhedron", "struct__Param__Polyhedron.html", "struct__Param__Polyhedron" ],
    [ "_Param_Vertex", "struct__Param__Vertex.html", "struct__Param__Vertex" ],
    [ "_Polyhedron_union", "struct__Polyhedron__union.html", "struct__Polyhedron__union" ],
    [ "Ehrhart", "classEhrhart.html", null ],
    [ "factor", "structfactor.html", "structfactor" ],
    [ "forsimplify", "structforsimplify.html", "structforsimplify" ],
    [ "interval", "structinterval.html", "structinterval" ],
    [ "LatticeUnion", "structLatticeUnion.html", "structLatticeUnion" ],
    [ "linear_exception_holder", "structlinear__exception__holder.html", "structlinear__exception__holder" ],
    [ "matrix", "structmatrix.html", "structmatrix" ],
    [ "polyhedron", "structpolyhedron.html", "structpolyhedron" ],
    [ "SatMatrix", "structSatMatrix.html", "structSatMatrix" ],
    [ "Vector", "structVector.html", "structVector" ],
    [ "ZPolyhedron", "structZPolyhedron.html", "structZPolyhedron" ]
];