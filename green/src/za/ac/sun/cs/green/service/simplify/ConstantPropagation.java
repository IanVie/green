package za.ac.sun.cs.green.service.simplify;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ConstantPropagation {
	//Propagation of one constant

	public static String propagation(String one){
		String fin = " ";
		ArrayList<String> sub=new ArrayList<String>();
		ArrayList<String> newSub=new ArrayList<String>();
		ArrayList<String> finalList =new ArrayList<String>();

		ArrayList<String> symbole = new ArrayList<String>();
		ArrayList<String> value = new ArrayList<String>();
		ArrayList<Integer> remove = new ArrayList<Integer>();

		StringTokenizer st = new StringTokenizer(one,"&&");  
		while (st.hasMoreTokens()) {
			sub.add(st.nextToken());
		} 
		for(int i =0;i<sub.size();i++){
			if(sub.get(i).contains("==")){
				symbole.add((sub.get(i).substring(0, sub.get(i).indexOf('='))).trim());
				value.add((sub.get(i).substring(sub.get(i).indexOf('=')+2)).trim());
				remove.add(i);
			}

		}
		for(int i =0;i<sub.size();i++){
			for(int j =0;j<symbole.size();j++){
				if(!sub.get(i).trim().contains("==")){
					fin = sub.get(i).trim().replaceAll(symbole.get(j), value.get(j));
					newSub.add(fin);

				}
			}
		}	
		for(int i=0;i<newSub.size();i++){
			for(int j =0 ;j<symbole.size();j++ ){
				if(newSub.get(i).contains(symbole.get(j))){
					finalList.add(newSub.get(i).trim().replaceAll(symbole.get(j), value.get(j)));
				}
			}

		}
		if(finalList.size()!=0){
			fin = finalList.get(0);
		}
		
		String arr [] = fin.split(" ");	
		return fin;
	}
	public static void main(String[] args) {
		propagation("y == x + 1 && x + y > 4");
	}

}
