\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}


%opening
\title{Tutorial 1 : Day One at Deep Solving Inc}
\author{17332001: IM Vurayai}


\begin{document}

\maketitle


\section{System requirements}
\subsection{Prerequisits requirements}
\begin{itemize}
  \item Ubuntu  10.04 or better
  \item IDE, Eclipse recommended 
  \item Basic knowledge of bash scripting 
\end{itemize}


\subsection{Installation Requirements }
\begin{itemize}
  \item Java 8 and  JUNIT Test
  \item Z3 and Barvinok
  \item Redis
\end{itemize}

\section{How to install software dependencies}
\subsection{Java 8}

 Install JAVA 8\footnote{https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK\_Howto.html}\\ 
\indent \texttt{\$ sudo add-apt-repository ppa:webupd8team/java} \\
\indent \texttt{\$ sudo apt-get update} \\
\indent \texttt{\$ sudo apt-get install oracle-java8-installer}\\
\indent \texttt{\$ sudo apt-get install oracle-java8-set-default} \\
\indent \texttt{\$ JAVA\_HOME=/usr/lib/jvm/java-8-oracle}\\
\indent \texttt{\$ JRE\_HOME=/usr/lib/jvm/java-8-oracle/jre}\\
\indent \texttt{\$ java -version } should output \texttt{java version "1.8.0\_131"}
\subsection{JUNIT}
 Install package junit\footnote{https://www.howtoinstall.co/en/ubuntu/trusty/junit4}\\ 
\indent \texttt{\$ sudo gedit /etc/apt/sources.list}\\
\indent \texttt{\$ deb http://us.archive.ubuntu.com/ubuntu xenial main universe}\\
\indent \texttt{\$ sudo apt-get update; sudo apt-get install junit}\\
\indent \texttt{\$ java -cp /usr/share/java/junit.jar junit.runner.Version} show \texttt{3.8.2}\\

\subsection{Z3}
Install Z3 package\footnote{https://github.com/Z3Prover/z3} \\
\indent \texttt{\$ git clone https://github.com/Z3Prover/z3.git}\\
\indent \texttt{\$ tar xvf z3-master.zip}\\
\indent \texttt{\$ cd z3-master}\\
\indent \texttt{\$ python scripts/mk\_make.py --java}\\
\indent \texttt{\$ cd build}\\
\indent \texttt{\$ make}\\
\indent \texttt{\$ sudo make install}\\
\indent \texttt{\$ export LD\_LIBRARY\_PATH=\$LD\_LIBRARY\_PATH:libz3java.so}\\
\subsection{Barvinok}
Install Z3 package\footnote{https://github.com/catch22/barvikron/blob/master/README.md} \\
\indent \texttt{\$ git clone }\\
\indent \texttt{\$ git submodule init}\\
\indent \texttt{\$ git submodule update}\\
\indent \texttt{\$ sh autogen.sh}\\
\indent \texttt{\$ NTL\_GMP\_LIP=on; NTL\_STD\_CXX=on}\\
\indent \texttt{\$ cd src}\\
\indent \texttt{\$ ./configure NTL\_GMP\_LIP=on PREFIX=/opt GMP\_PREFIX=/path/to/gm}\\
\indent \texttt{\$ make}\\
\indent \texttt{\$ make install}\\
\indent \texttt{\$ ./configure --prefix=/opt --with-gmp-prefix=/path/to/gmp --with-ntl-prefix=/opt}\\

\subsection{ Redis, caches all the results that it calculates}
Install Redis as a cache on Ubuntu \footnote{https://redis.io/topics/quickstart}\\
\indent \texttt{\$ sudo apt-get install redis-server}\\
Add the following below lines to \texttt{/etc/redis/redis.conf}\\
\texttt{maxmemory 128mb}\\
\texttt{maxmemory-policy allkeys-lru}\\
\indent \texttt{\$ sudo systemctl restart redis-server.service}\\
\indent \texttt{\$ sudo systemctl enable redis-server.service}\\

\end{document}
